# Styling syntax highlighting

This folder contains all the color mappings used for syntax highlighting.

# Template (for creating new styles)

```scss
/*
* [Template] Syntax Colors
* Token objects map to https://github.com/jneen/rouge/wiki/List-of-tokens
*/

/*
* Each token has the following optional properties
*  "color": null,
*  "background": null,
*  "bold": false,
*  "italic": false,
*
* For nested token example, see white.scss
*/

$template-highlight-selectors-keyword: (
  "default": (
    "token": "k",
  ),
  "constant": (
    "token": "kc",
  ),
  "declaration": (
    "token": "kd",
  ),
  "namespace": (
    "token": "kn",
  ),
  "pseudo": (
    "token": "kp",
  ),
  "reserved": (
    "token": "kr",
  ),
  "type": (
    "token": "kt",
  ),
);

$template-highlight-selectors-name: (
  "default": (
    "token": "n",
  ),
  "attribute": (
    "token": "na",
  ),
  "builtin": (
    "token": "nb",
  ),
  "builtin.pseudo": (
    "token": "np",
  ),
  "class": (
    "token": "nc",
  ),
  "constant": (
    "token": "no",
  ),
  "decorator": (
    "token": "nd",
  ),
  "entity": (
    "token": "ni",
  ),
  "exception": (
    "token": "ne",
  ),
  "function": (
    "token": "nf",
  ),
  "property": (
    "token": "py",
  ),
  "label": (
    "token": "nl",
  ),
  "namespace": (
    "token": "nn",
  ),
  "other": (
    "token": "nx",
  ),
  "tag": (
    "token": "nt",
  ),
  "variable": (
    "token": "nv",
  ),
  "variable.class": (
    "token": "vc",
  ),
  "variable.global": (
    "token": "vg",
  ),
  "variable.instance": (
    "token": "vi",
  ),
);

$template-highlight-selectors-literal: (
  "default": (
    "token": "l",
  ),
  "date": (
    "token": "ld",
  ),
);

$template-highlight-selectors-literal-string: (
  "default": (
    "token": "s",
  ),
  "backtick": (
    "token": "sb",
  ),
  "char": (
    "token": "sc",
  ),
  "doc": (
    "token": "sd",
  ),
  "double": (
    "token": "s2",
  ),
  "escape": (
    "token": "se",
  ),
  "heredoc": (
    "token": "sh",
  ),
  "interpol": (
    "token": "si",
  ),
  "other": (
    "token": "sx",
  ),
  "regex": (
    "token": "sr",
  ),
  "single": (
    "token": "s1",
  ),
  "symbol": (
    "token": "ss",
  ),
);

$template-highlight-selectors-literal-number: (
  "default": (
    "token": "m",
  ),
  "float": (
    "token": "mf",
  ),
  "hex-number": (
    "token": "mh",
  ),
  "integer": (
    "token": "mi",
  ),
  "integer.long": (
    "token": "il",
  ),
  "oct": (
    "token": "mo",
  ),
  "hex-literal": (
    "token": "mx",
  ),
  "bin": (
    "token": "mb",
  )
);

$template-highlight-selectors-comment: (
  "default": (
    "token": "c",
  ),
  "multiline": (
    "token": "cm",
  ),
  "preproc": (
    "token": "cp",
  ),
  "single": (
    "token": "c1",
  ),
  "special": (
    "token": "cs",
  ),
);

$template-highlight-selectors-generic: (
  "default": (
    "token": "g",
  ),
  "deleted": (
    "token": "gd",
  ),
  "emph": (
    "token": "ge",
  ),
  "error": (
    "token": "gr",
  ),
  "heading": (
    "token": "gh",
  ),
  "inserted": (
    "token": "gi",
  ),
  "output": (
    "token": "go",
  ),
  "prompt": (
    "token": "gp",
  ),
  "strong": (
    "token": "gs",
  ),
  "subheading": (
    "token": "gu",
  ),
  "traceback": (
    "token": "gt",
  ),
  "lineno": (
    "token": "gl",
  )
);

$template-highlight-selectors-misc: (
  "text.whitespace": (
    "token": "w",
  ),
  "error": (
    "token": "err",
  ),
  "other": (
    "token": "x",
  ),
  "operator": (
    "token": "o",
  ),
  "operator.word": (
    "token": "ow",
  ),
  "punctuation": (
    "token": "p",
  ),
);

$template-highlight-selectors: (
  "keyword": $template-highlight-selectors-keyword,
  "name": $template-highlight-selectors-name,
  "literal": $template-highlight-selectors-literal,
  "literal.string": $template-highlight-selectors-literal-string,
  "literal.number": $template-highlight-selectors-literal-number,
  "comment": $template-highlight-selectors-comment,
  "generic": $template-highlight-selectors-generic,
  "misc": $template-highlight-selectors-misc,
);

$template-highlight-background: null;
$template-highlight-text-color: null;
```
