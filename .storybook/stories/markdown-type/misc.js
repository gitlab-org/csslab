export default {
  anchor: () => `
    <section class="md">
      <p>
        This is an example of <a href="#">an anchor</a>.
      </p>
    </section>
  `,
  anchorMonospace: () => `
    <section class="md">
      <p>
        This is an example of a monospace anchor. <a class="monospace" href="#">9ba12248...b19a04f5</a>.
      </p>
    </section>
  `,
  mention: () => `
    <section class="md">
      <p>
        This is an example of a username. <a class="mention" href="#">@username</a>
      </p>
    </section>
  `,
  codeSnippet: () => `
    <section class="md">
      <p>
        <code>Markdown code snippet</code>
      </p>
    </section>
  `,
  codeSnippetLink: () => `
    <section class="md">
      <p>
        <a href="#">
          This is a <code>code snippet link</code> to test
        </a>
      </p>
    </section>
  `,
  colorChip: () => `
    <section class="md">
      <p>
        <code>
          #ffffff
          <span class="color-chip" style="background-color:#ffffff"></span>
        </code>
      </p>
    </section>
  `,
  blockquote: () => `
    <section class="md">
      <p>
        <blockquote>
          <p>
            This is an example of a blockquote. It extends the standard Markdown in a few significant ways to add some useful functionality. You can use GFM in the following areas: comments, issues, merge requests, milestones, snippets (the snippet must be named with a .md extension), wiki pages, and markdown documents inside the repository.
          </p>
        </blockquote>
      </p>
    </section>
  `,
  additionDeletion: () => `
    <section class="md">
      <p>
        This is an example of an <span class="addition">addition</span>. This is an example of a <span class="deletion">deletion</span>
      </p>
    </section>
  `,
  emoji: () => `
    <section class="md">
      <h1>
        Markdown h1 <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </h1>
      <h2>
        Markdown h2 <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </h2>
      <h3>
        Markdown h3 <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </h3>
      <h4>
        Markdown h4 <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </h4>
      <h5>
        Markdown h5 <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </h5>
      <p>
        This is an example of an emoji <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji> within a sentence.
      </p>
      <p>
        <gl-emoji title="thumbs up sign" data-name="thumbsup" data-unicode-version="6.0">👍</gl-emoji>
      </p>
    </section>
  `,
  table: () => `
    <section class="md">
      <table dir="auto">
        <thead>
          <tr>
            <th>Header 01</th>
            <th>Header 02</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>column</td>
            <td>column</td>
          </tr>
        </tbody>
      </table>
      <table dir="auto">
        <thead>
          <tr>
            <th>Header 01</th>
            <th>Header 02</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>column</td>
            <td>GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality.</td>
          </tr>
        </tbody>
      </table>
    </section>
  `,
};
