import sample from './sample.html';

const themeSample = (theme) => `
  <section class="md">
    <pre class="code highlight js-syntax-highlight javascript ${theme}" lang="javascript" v-pre="true">
    ${sample}
    </pre>
  </section>
`

export default {
  white: () => themeSample('white'),
  dark: () => themeSample('dark'),
  monokai: () => themeSample('monokai'),
  solarizedLight: () => themeSample('solarized-light'),
  solarizedDark: () => themeSample('solarized-dark'),
}
