export default {
  unordered: () => `
    <section class="md">
      <p>
        <ul dir="auto">
          <li>List item</li>
          <li>List item
            <ul>
              <li>List item</li>
              <li>List item</li>
            </ul>
          </li>
          <li>List item</li>
        </ul>
      </p>
    </section>
  `,
  ordered: () => `
    <section class="md">
      <p>
        <ol dir="auto">
          <li>List item</li>
          <li>List item
            <ol>
              <li>List item</li>
              <li>List item</li>
            </ol>
          </li>
          <li>List item</li>
        </ol>
      </p>
    </section>
  `,
  task: () => `
    <section class="md">
      <p>
        <ul class="task-list" dir="auto">
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item
            <ul class="task-list">
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
            </ul>
          </li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
        </ul>

        <ul class="task-list" dir="auto">
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox" disabled>Disabled list item</li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox" disabled>Disabled list item
            <ul class="task-list">
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox" disabled>Disabled list item</li>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox" disabled>Disabled list item</li>
            </ul>
          </li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox" disabled>Disabled list item</li>
        </ul>

        <ul class="task-list" dir="auto">
          <li>List item
            <ul>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item
                <ul>
                  <li>List item
                    <ul>
                      <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>

        <ul class="task-list" dir="auto">
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item
            <ul>
              <li>List item
                <ul>
                  <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item
                    <ul>
                      <li>List item
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>

        <ol class="task-list" dir="auto">
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item
            <ol>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
            </ol>
          </li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">List item</li>
        </ol>
      </p>
    </section>
  `,
  rtl: () => `
    <section class="md">
      <p>
        <ul dir="rtl">
          <li>قائمة الاغراض</li>
          <li>قائمة الاغراض
            <ul dir="rtl">
              <li>قائمة الاغراض</li>
              <li>قائمة الاغراض</li>
            </ul>
          </li>
          <li>قائمة الاغراض</li>
        </ul>

        <ol dir="rtl">
          <li>قائمة الاغراض</li>
          <li>قائمة الاغراض
            <ol dir="rtl">
              <li>قائمة الاغراض</li>
              <li>قائمة الاغراض</li>
            </ol>
          </li>
          <li>قائمة الاغراض</li>
        </ol>

        <ul class="task-list" dir="rtl">
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">قائمة الاغراض</li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">قائمة الاغراض
            <ul class="task-list" dir="rtl">
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">قائمة الاغراض</li>
              <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">قائمة الاغراض</li>
            </ul>
          </li>
          <li class="task-list-item"><input type="checkbox" class="task-list-item-checkbox">قائمة الاغراض</li>
        </ul>
      </p>
    </section>
  `,
};
