export default {
  h1: () => `
    <section class="md">
      <h1>
        # Markdown h1 • GitLab uses "GitLab Flavored Markdown" (GFM).
      </h1>
    </section>
  `,
  h2: () => `
    <section class="md">
      <h2>
        ## Markdown h2 • GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality.
      </h2>
    </section>
  `,
  h3: () => `
    <section class="md">
      <h3>
        ### Markdown h3 • GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality.
      </h3>
    </section>
  `,
  h4: () => `
    <section class="md">
      <h4>
        #### Markdown h4 • GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality.
      </h4>
    </section>
  `,
  h5: () => `
    <section class="md">
      <h5>
        ##### Markdown h5 • GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality. You can use GFM in the following areas: comments, issues, merge requests, milestones, snippets, wiki pages, and markdown documents inside the repository.
      </h5>
    </section>
  `,
  h6: () => `
    <section class="md">
      <h6>
        ###### Markdown h6 • GitLab uses "GitLab Flavored Markdown" (GFM). It extends the standard Markdown in a few significant ways to add some useful functionality. You can use GFM in the following areas: comments, issues, merge requests, milestones, and more.
      </h6>
    </section>
  `,
};
