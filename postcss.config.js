'use strict'

module.exports = () => ({
  map: {
    inline: false,
    annotation: true,
    sourcesContent: false
  },
  plugins: [
    require('@csstools/postcss-sass')({
        includePaths: ['node_modules/bootstrap-sass/assets/stylesheets/']
    }),
    require('autoprefixer')({
        cascade: false
    })
  ],
});
